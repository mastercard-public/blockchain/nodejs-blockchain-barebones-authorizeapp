#!/usr/bin/env node
var blockchain = require('mastercard-blockchain');
var MasterCardAPI = blockchain.MasterCardAPI;

const async = require('async'), encoding = 'hex', fs = require('fs');
var prompt = require('prompt'), options = createOptions();

var protobuf = require("protobufjs");

var argv = options.argv;
prompt.override = argv;

var protoFile = null;
var appID = null;
var msgClassDef = null;

console.log(fs.readFileSync('help.txt').toString());
prompt.start();
initApi((err, result) => {
  if (!err) {
    updateNode((error, data) => {
      processCommands();
    });
  }
});

function processCommands() {
  async.waterfall([
    function (callback) {
      prompt.get(promptCmdSchema(), callback);
    }
  ], function (err, result) {
    if (!err) {
      switch (result.cmdOption) {
        case 1:
          createNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 2:
          updateNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 3:
          createAuthorize((error, data) => {
            processCommandsAfter();
          });
          break;
        case 4:
          fs.readFile(protoFile, (err, data) => {
            if (err) {
              console.log('error', err);
            } else {
              console.log(data.toString());
            }
            processCommandsAfter();
          });
          break;
        case 5:
          initApi((err, result) => {
            if (!err) {
              processCommandsAfter();
            }
          });
          break;
        case 6:
          options.showHelp();
          async.nextTick(processCommandsAfter);
          break;
        default:
          console.log('Goodbye');
          break;
      }
    }
  });
}

function createNode(callback) {
  console.log('createNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'application.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        blockchain.Node.provision({
          network: 'ZONE',
          application: {
            name: appID,
            description: "",
            version: 0,
            definition: {
              format: "proto3",
              encoding: 'base64',
              messages: fs.readFileSync(protoFile).toString('base64')
            }
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function updateNode(callback) {
  console.log('updateNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'application.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        blockchain.App.update({
          id: appID,
          name: appID,
          description: "",
          version: 0,
          definition: {
            format: "proto3",
            encoding: 'base64',
            messages: fs.readFileSync(protoFile).toString('base64')
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    },
    function (result, callback) {
      blockchain.App.read(appID, {}, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}


function createAuthorize(callback) {
  console.log('createAuthorize');
  async.waterfall([
    function (callback) {
      prompt.get(promptAuthorizeEntrySchema(), callback);
    },
    function (data, callback) {
      blockchain.Authorize.create({
        amountMinorUnits: parseInt(data.amount),
        currency: data.currency,
        pan: data.pan,
        description: data.description,
        pos_id: data.posId,
        merchant_id: data.merchantId
      }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function initApi(onDone) {
  console.log('initializing');
  async.waterfall([
    function (callback) {
      prompt.get(promptInitSchema(), callback);
    },
    function (result, callback) {
      var authentication = new MasterCardAPI.OAuth(result.consumerKey, result.keystorePath, result.keyAlias, result.storePass);
      MasterCardAPI.init({
        sandbox: true,
        debug: argv.verbosity,
        authentication: authentication
      });
      protoFile = result.protoFile;
      protobuf.load(protoFile, callback);
    }
  ], function (err, root) {
    if (err) {
      console.log('error', err);
    } else {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        console.log('initialized');
      } else {
        console.log('could not read message class def from', protoFile);
      }
    }
    async.nextTick(onDone, err, appID);
  });
}

function processCommandsAfter() {
  async.waterfall([
    function (callback) {
      prompt.get({ properties: { enter: { description: 'press enter to continue', required: false } } }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    }
    async.nextTick(processCommands);
  });
}

function promptInitSchema() {
  return {
    properties: {
      keystorePath: {
        description: 'the path to your keystore (mastercard developers)',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      },
      storePass: {
        description: 'keystore password (mastercard developers)',
        required: true,
        default: 'keystorepassword'
      },
      consumerKey: {
        description: 'consumer key (mastercard developers)',
        required: true
      },
      keyAlias: {
        description: 'key alias (mastercard developers)',
        required: true,
        default: 'keyalias'
      },
      protoFile: {
        description: 'the path to the protobuf File',
        required: true,
        default: 'application.proto'
      }
    }
  };
}

function promptAuthorizeEntrySchema() {
  return {
    properties: {
      amount: {
        description: 'amount',
        required: true,
        default: '1234',
        conform: (value) => {
          return parseInt(value) > 0;
        }
      },
      currency: {
        description: 'currency',
        required: true,
        default: 'USD'
      },
      pan: {
        description: 'pan',
        required: true,
        default: '5454545454545454'
      },
      description: {
        description: 'description',
        required: true,
        default: 'This is a sample authorization request'
      },
      posId: {
        description: 'pos id',
        required: true,
        default: '20801234-1234-5678-9012-1EC23E4E5678'
      },
      merchantId: {
        description: 'merchant id',
        required: true,
        default: 'ABCDEF5xg9PZJ3WghIJklMnOP1qrstUVm7'
      }
    }
  };
}

function promptCmdSchema() {
  return {
    properties: {
      cmdOption: {
        description: '============ MENU ============\n1. Create node (optional, onetime)\n2. Update protocol buffer definition\n3. Create authorization request\n4. Show Protocol Buffer Definition\n5. Re-initialize API\n6. Print Command Line Options\n0. Quit\nOption',
        message: 'Invalid Option',
        type: 'number',
        default: 0,
        required: true,
        conform: (value) => {
          return value >= 0 && value <= 6;
        }
      }
    }
  };
}

function createOptions() {
  return require('yargs')
    .options({
      'consumerKey': {
        alias: 'ck',
        description: 'consumer key (mastercard developers)'
      },
      'keystorePath': {
        alias: 'kp',
        description: 'the path to your keystore (mastercard developers)'
      },
      'keyAlias': {
        alias: 'ka',
        description: 'key alias (mastercard developers)'
      },
      'storePass': {
        alias: 'sp',
        description: 'keystore password (mastercard developers)'
      },
      'protoFile': {
        alias: 'pf',
        description: 'protobuf file'
      },
      'verbosity': {
        alias: 'v',
        default: false,
        description: 'log mastercard developers sdk to console'
      }
    });
}

function getProperties(obj) {
  var ret = [];
  for (var name in obj) {
    if (obj.hasOwnProperty(name)) {
      ret.push(name);
    }
  }
  return ret;
}

function guessNested(root) {
  var props = getProperties(root.nested);
  var firstChild = getProperties(root.nested[props[0]].nested);
  return [props[0], firstChild[0]];
}
